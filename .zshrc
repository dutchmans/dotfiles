# Configure PS1 terminal and colors
PS1="%n@%M %~ $ "
wal -CnR > /dev/null
alias ls="lsd"
alias vifm="vifmrun"

# Auto-completion stuff
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Include hidden files in autocomplete:
_comp_options+=(globdots)

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
